var readBooksPromise = require('./promise.js')

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 }
]

function exec(times, index) {
  readBooksPromise(times, books[index])
    .then(timesRemain => {
      if (timesRemain !== 0) {
        exec(timesRemain, index + 1)
      }
    })
    .catch(error => console.log('Hmmm..something went wrong ? ' + error))
}

exec(10000, 0)