var readBooks = require('./callback.js')

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'Komik', timeSpent: 1000 }
]

function exec(times, index) {
  readBooks(times, books[index], function(timesRemain) {
    if (timesRemain !== 0) {
      exec(timesRemain, index + 1)
    } else {
      console.log('Waktu saya habis')
    }
  })
}

exec(10000, 0)