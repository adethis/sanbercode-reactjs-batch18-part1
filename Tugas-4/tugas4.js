// Soal 1
var index = 1
var total = 20
console.log('LOOPING PERTAMA')
while (index <= total) {
	if (index % 2 == 0) {
		console.log(index + ' - I love coding')
	}
	++index
}
console.log('LOOPING KEDUA')
while (total >= 1) {
	if (total % 2 == 0) {
		console.log(total + ' - I will become a frontend developer')
	}
	--total
}

console.log('\n')


// Soal 2
for (var i = 1; i <= 20; i++) {
	if (i % 2 == 0) {
		console.log(i + ' - Berkualitas')
	} else if (i % 3 == 0) {
		console.log(i + ' - I love code')
	} else if (i % 2 != 0) {
		console.log(i + ' - Santai')
	}
}


console.log('\n')


// Soal 3
var tag = ''
for (var i = 1; i <= 7; i++) {
	console.log(tag += '#')
}


console.log('\n')


// Soal 4
var kalimat = "saya sangat senang belajar javascript"
console.log(kalimat.split(" "))


console.log('\n')


// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"]
for (var i = 0; i < daftarBuah.sort().length; i++) {
	console.log(daftarBuah[i])
}