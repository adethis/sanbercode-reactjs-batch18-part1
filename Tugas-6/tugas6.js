// Soal 1
const luasLingkaran = jari => {
  let pi = jari % 7 === 0 ? 22/7 : 3.14
  return pi * jari * jari
}
const kelilingLingkaran = jari => {
  let pi = jari % 7 === 0 ? 22/7 : 3.14
  return 2 * pi * jari
}
console.log(luasLingkaran(5))
console.log(kelilingLingkaran(14))
console.log('\n')



// Soal 2
let kalimat = ''
const tambahKata = kata => {
  kalimat += `${kata} `
}
tambahKata('saya')
tambahKata('adalah')
tambahKata('seorang')
tambahKata('frontend')
tambahKata('developer')
console.log(kalimat)
console.log('\n')



// Soal 3
const newFunction = function literal(firstName, lastName) {
  return {
    firstName,
    lastName,
    fullName() {
      console.log(firstName + ' ' + lastName)
      return
    }
  }
}
newFunction("William", "Imoh").fullName()
console.log('\n')



// Soal 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Dave-wizard Avocado",
  spell: "Vimlus Renderus!!!"
}
const { firstName, lastName, destination, occupation } = newObject
console.log(firstName, lastName, destination, occupation)
console.log('\n')



// Soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)
console.log('\n')