// Soal 1
function halo() {
	return 'Halo Sanbers!'
}
console.log(halo())
console.log('\n')



// Soal 2
function kalikan(param1, param2) {
	return param1 * param2
}
var num1 = 12
var num2 = 4
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)
console.log('\n')



// Soal 3
function introduce(parname, parage, paraddress, parhobby) {
	return 'Nama saya ' + parname + ', umur saya ' + parage + ', alamat saya ' + paraddress + ', dan saya punya hobi yaitu ' + parhobby
}
var name = 'Steve'
var age = 29
var address = 'Seatle NY'
var hobby = 'Sleeping'
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
console.log('\n')


// Soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992]
var objDaftarPeserta = {}
objDaftarPeserta.name = arrayDaftarPeserta[0]
objDaftarPeserta.jenis_kelamin = arrayDaftarPeserta[1]
objDaftarPeserta.hobi = arrayDaftarPeserta[2]
objDaftarPeserta.tahun_lahir = arrayDaftarPeserta[3]
console.log(objDaftarPeserta)
console.log('\n')



// Soal 5
var buah = [
	{
		"nama": "Strawberry",
		"warna": "Merah",
		"ada bijinya" : "tidak",
		"harga": 9000
	},
	{
		"nama": "Jeruk",
		"warna": "Oranye",
		"ada bijinya" : "ada",
		"harga": 8000
	},
	{
		"nama": "Semangka",
		"warna": "Hijau & merah",
		"ada bijinya" : "ada",
		"harga": 10000
	},
	{
		"nama": "Pisang",
		"warna": "Merah",
		"ada bijinya" : "tidak",
		"harga": 5000
	}
]
console.log(buah[0])



// Soal 6
var dataFilm = []
function insertFilm(nama, durasi, genre, tahun) {
	var film = { nama, durasi, genre, tahun }
	dataFilm.push(film)
}
insertFilm('Harry Potter', 190, 'Fiction', 2008)
insertFilm('Ice Age', 90, 'Fiction', 2011)
console.log(dataFilm)