var kataPertama, 
	kataKedua, 
	kataKetiga, 
	kataKeempat, 
	total, 
	kalimat, 
	nilai, 
	tanggal, 
	bulan, 
	tahun
;

// Soal 1
kataPertama = "saya";
kataKedua = "senang";
kataKetiga = "belajar";
kataKeempat = "javascript";

kalimat = kataPertama.concat(" ", kataKedua.charAt(0).toUpperCase()+kataKedua.substr(1), " ", kataKetiga, " ", kataKeempat.toUpperCase());

console.log(kalimat);


// Soal 2
kataPertama = "1";
kataKedua = "2";
kataKetiga = "3";
kataKeempat = "5";

total = parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat);

console.log(total)


// Soal 3
kalimat = 'wah javascript itu keren sekali';

kataPertama = kalimat.substring(0, 3);
kataKedua = kalimat.substr(4, 10);
kataKetiga = kalimat.substr(15, 3);
kataKeempat = kalimat.substr(18, 6);
kataKelima = kalimat.substring(24);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);


// Soal 4
nilai = 91;

if (nilai >= 80) {
	console.log('A');
} else if (nilai >= 70 && nilai < 80) {
	console.log('B');
} else if (nilai >= 60 && nilai < 70) {
	console.log('C');
} else if (nilai >= 50 && nilai < 60) {
	console.log('D');
} else if (nilai < 50) {
	console.log('E');
}


// Soal 5
tanggal = 16;
bulan = 9;
tahun = 1991;

switch(bulan) {
	case 1: { bulan = ' Januari '; break; }
	case 2: { bulan = ' Februari '; break; }
	case 3: { bulan = ' Maret '; break; }
	case 4: { bulan = ' April '; break; }
	case 5: { bulan = ' Mei '; break; }
	case 6: { bulan = ' Juni '; break; }
	case 7: { bulan = ' Juli '; break; }
	case 8: { bulan = ' Agustus '; break; }
	case 9: { bulan = ' September '; break; }
	case 10: { bulan = ' Oktober '; break; }
	case 11: { bulan = ' November '; break; }
	case 12: { bulan = ' Desember '; break; }
	default: { bulan = ' bulan tidak ditemukan '; }
}

console.log(tanggal + bulan + tahun);
